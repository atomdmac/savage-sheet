import { Trait } from './traits';

export const calculateSkillCost = (attributeTraits: Trait[], skill: Trait) => {
  const linked = attributeTraits.find((at: Trait) => at.name === skill.linked);
  if (!linked) {
    return 0;
  }

  const baseCost = skill.isCore ? 0 : ((skill.die - 2) / 2);
  const bonusCost = Math.max(0, (skill.die - linked.die)) / 2;

  return baseCost + bonusCost;
}

export const calculateTotalSkillCost = (attributeTraits: Trait[], skills: Trait[]) => {
  return skills.reduce((total: number, skill: Trait) => {
    return total + calculateSkillCost(attributeTraits, skill)
  }, 0)
}

export const calculateTotalAttributeCost = (attributeTraits: Trait[]) => {
  return attributeTraits.reduce((total: number, attribute: Trait) => {
    return total + ((attribute.die - 4) / 2);
  }, 0);
}


import React from 'react';
import { Die } from './traits';

export interface DieSelectorProps {
  onChange: (e: any) => void
  die: Die
}

export const DieSelector = (props: DieSelectorProps) => {
  const onChange = (e: any) => props.onChange(parseInt(e.target.value))
  return (
    <select value={props.die} onChange={onChange}>
      <option value="4">d4</option>
      <option value="6">d6</option>
      <option value="8">d8</option>
      <option value="10">d10</option>
      <option value="12">d12</option>
    </select>
  )
}

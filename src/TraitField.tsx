import React from 'react';
import { Trait } from './traits';
import { DieSelector } from './DieSelector'
import _ from 'lodash';

export interface TraitFieldProps {
  onChange?: (e: any, trait: Trait) => void
  trait: Trait
}

export const TraitField = (props: TraitFieldProps) => {
  const onChange = (e: any) => (props.onChange || _.noop)(e, props.trait)
  return (
    <div className="trait-field">
      <label>{props.trait.name}:</label>
      <DieSelector onChange={onChange} die={props.trait.die} />
      <input type="number" value={props.trait.modifier} />
    </div>
  );
}

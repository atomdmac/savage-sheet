import React from 'react';
import { useState } from 'react'
import { attributes, skills, Die, Trait } from './traits';
import { TraitField } from './TraitField'
import {
  calculateTotalAttributeCost,
  calculateTotalSkillCost
} from './calculators';
import './CharacterSheet.css';

function CharacterSheet() {
  const [attributesState, setAttributesState] = useState(attributes)
  const [skillsState, setSkillsState] = useState(skills)

  const [attributePoints, setAttributePoints] = useState(
    calculateTotalAttributeCost(attributesState)
  );
  const [skillPoints, setSkillPoints] = useState(
    calculateTotalSkillCost(attributesState, skillsState)
  );

  const onSkillChange = (newValue: Die, trait: Trait) => {
    const newTrait = {
      ...trait,
      die: newValue
    }

    const newSkills = (skillsState.map((s: Trait) => {
      return s.name !== trait.name
        ? s
        : newTrait
    }))

    setSkillsState(newSkills)

    setSkillPoints(
      calculateTotalSkillCost(attributesState, newSkills)
    );
  }

  const onAttributeChange = (newValue: Die, trait: Trait) => {
    const newTrait = {
      ...trait,
      die: newValue
    }

    const newAttributes = (attributesState.map((s: Trait) => {
      return s.name !== trait.name
        ? s
        : newTrait
    }))

    setAttributesState(newAttributes);
    setAttributePoints(
      calculateTotalAttributeCost(newAttributes)
    );
    setSkillPoints(
      calculateTotalSkillCost(newAttributes, skillsState)
    );
  }

  const attributeEls = attributesState
    .map((attribute) => (
      <li key={attribute.name} className="trait">
        <TraitField onChange={onAttributeChange} trait={attribute}/>
      </li>
    ))
  const skillsEls = skillsState
    .map((skill) => (
      <li key={skill.name} className="trait">
        <TraitField onChange={onSkillChange} trait={skill}/>
      </li>
    ))
  return (
    <div className="character-sheet">
      <h1>Attribute Points: {attributePoints}</h1>
      <h1>Skill Points: {skillPoints}</h1>
      <ul>
        {attributeEls}
      </ul>
      <ul>
        {skillsEls}
      </ul>
    </div>
  );
}

export default CharacterSheet;
